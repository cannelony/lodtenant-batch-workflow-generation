(function(){
  'use strict';

  // Prepare the 'workflow' module for subsequent registration of controllers and delegates
  angular.module('workflow', [ 'ngMaterial' ]);

})();
