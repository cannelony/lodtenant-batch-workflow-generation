(function(){

    angular
        .module('workflow')
        .controller('WorkflowController', [
            '$scope', '$mdSidenav', '$mdBottomSheet', '$log', '$q',
            WorkflowController
        ]);

    /**
     * Main Controller for the Lodtenant App
     * @param $scope
     * @param $mdSidenav
     * @constructor
     */
    function WorkflowController( $scope, $mdSidenav, $mdBottomSheet, $log, $q) {
        var self = this;

        // 0: Prefixes 1: Workflow definition
        $scope.menuEntry = 0;

        $scope.prefixes = [{}];
        $scope.beans = [{}];
        $scope.jobs = [{ steps: [{}] }];
        $scope.misc = {
            todoTrim: '',
            httpUserAgent: ''
        };

        self.selected        = null;
        self.workflowJS      = genJson;
        self.toggleList      = toggleWorkflowConfiguration;
        self.removeArrayItem = removeItem;
        self.reset           = resetConfiguration;
        self.example         = loadExample;
        self.stepOptionValue = setStepOptionValue;


        // *********************************
        // Internal methods
        // *********************************

        /**
         * First hide the bottomsheet IF visible, then
         * hide or Show the 'left' sideNav area
         */
        function toggleWorkflowConfiguration() {
            var pending = $mdBottomSheet.hide() || $q.when(true);

            pending.then(function(){
                $mdSidenav('left').toggle();
            });
        }

        function genJson() {

            var workflow = {};

            workflow.todoTrim = $scope.misc.todoTrim;
            workflow.httpUserAgent = $scope.misc.httpUserAgent;


            // set prefixes
            workflow.prefixes = {
                '$prefixes' : {}
            };
            for (var pItem in $scope.prefixes) {
                var prefix = $scope.prefixes[pItem];
                workflow.prefixes.$prefixes[prefix.prefix] = prefix.iri;
            }

            // set bean types
            for (var bType in $scope.beans) {
                var bItem = $scope.beans[bType];
                var bTypeConf = {};
                bTypeConf[bItem.beanType] = [];
                bTypeConf[bItem.beanType].push(bItem.sparqlEndpointUri);
                bTypeConf[bItem.beanType].push(bItem.namedGraph);
                console.log('bean', bTypeConf);
                workflow[bItem.label] = bTypeConf;
            }

            //set jobs
            var jobs = angular.copy($scope.jobs);
            for (var jItem in jobs) {
                var job = jobs[jItem];
                var jobConf = {};
                jobConf.name = job.label;
                jobConf.steps = [];

                // add steps from job
                for (var sItem in job.steps) {
                    var step = job.steps[sItem];
                    // set source and target syntax for workflow config
                    step.source = '#{ ' + step.source + ' }';
                    step.target = '#{ ' + step.target + ' }';
                    jobConf.steps.push(step);
                }

                workflow.job = {};
                workflow.job.$simpleJob = jobConf;
            }
            console.log('Generated JSON', JSON.stringify(workflow));
            console.log('jobs', $scope.jobs);

            alert(JSON.stringify(workflow));

        }

        function removeItem(array,index) {
            array.splice(index,1);
            console.log('del item', index, array);
        }

        function resetConfiguration() {
            console.log('todoTrim reset', $scope);
            $scope.prefixes = [{}];
            $scope.beans = [{}];
            $scope.jobs = [{ steps: [{}] }];
            $scope.misc.todoTrim = '';
            $scope.misc.httpUserAgent = '';
        }

        function setStepOptionValue(val) {
            return '{{ ' + val + ' }}';
        }

        function loadExample() {
            $scope.misc = {
                todoTrim: "BIND('^\\s*(.*)\\s*$' AS ?trimPattern)",
                httpUserAgent: 'givenname.surname@example.org'
            };

            $scope.prefixes = [
                { prefix: 'foaf', iri: 'http://xmlns.com/foaf/0.1/' },
                { prefix: 'foo', iri: 'http://foobar/' }
            ];

            $scope.beans = [
                { label: 'source', beanType: '$sparqlService', sparqlEndpointUri: 'http://localhost:8890/sparql', namedGraph: 'http://fp7-pp.publicdata.eu/'},
                { label: 'resloc', beanType: '$sparqlService', sparqlEndpointUri: 'http://localhost:8890/sparql', namedGraph: 'http://fp7-pp.publicdata.eu/resloc'},
                { label: 'geocoderCache', beanType: '$sparqlService', sparqlEndpointUri: 'http://localhost:8890/sparql', namedGraph: 'http://fp7-pp.publicdata.eu/locjson/'},
                { label: 'target', beanType: '$sparqlService', sparqlEndpointUri: 'http://localhost:8890/sparql', namedGraph: 'http://fp7-pp.publicdata.eu/'}
            ];

            $scope.jobs = [
                {
                    beanType: '$simpleJob',
                    label: 'geoCodingJob',
                    steps: [
                        {
                            beanType: '$sparqlPipe',
                            label: 'createLocations',
                            chunk: 1000,
                            source: 'target',
                            target: 'resloc',
                            query: ''+
                                'CONSTRUCT { \n' +
                                '  ?s tmp:location ?l \n'+
                                '} \n'+
                                'WHERE { \n' +
                                '    ?s \n'+
                                '    o:address [\n'+
                                '        o:country [ rdfs:label ?col ] ; \n'+
                                '        o:city [ rdfs:label ?cil ] \n'+
                                '    ] \n'+
                                '    BIND(str(?cil) As ?clis) \n'+
                                '    BIND(concat(?cils, if(strlen(?cils) > 0, " ", ""), ?col) As ?l) \n'+
                                '}'
                        }
                    ]
                }
            ];
        }

    }

})();
